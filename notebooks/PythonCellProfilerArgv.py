#!/usr/bin/env python
# coding: utf-8

# %tb

# Import Cell Profiler Dependencies
import cellprofiler
import cellprofiler_core.preferences as cpprefs
import cellprofiler_core.module as cpm
import cellprofiler_core.pipeline as cpp
cpprefs.set_headless()

# Inject Image module used to inject OMERO image planes into Cell Profiler Pipeline
from cellprofiler_core.modules.injectimage import InjectImage

# Import OMERO Python BlitzGateway
import omero
from omero.gateway import BlitzGateway

from getpass import getpass

# Import Numpy
import numpy as np

# Import Python System Packages
import os
import tempfile
import pandas as pd
import warnings
from sys import argv

# Import Matplotlib
import matplotlib
import matplotlib.pyplot as plt

#Import other Packages
from ipywidgets import widgets
from IPython.display import display


print("import done!")


# ## Definitions and helper functions


def print_obj(obj, indent=0):
    """
    Helper method to display info about OMERO objects.
    Not all objects will have a "name" or owner field.
    """
    print("""%s%s:%s  Name:"%s" (owner=%s)""" % (
        " " * indent,
        obj.OMERO_CLASS,
        obj.getId(),
        obj.getName(),
        obj.getOwnerOmeName()))

# Definitions for selected plate info

def print_plateInfo(plate, indent=0):
    """
    Helper method to display info about OMERO plate selected before running Cp pipeline.
    
    """
    print("""%s%s:%s  Name:"%s" (owner=%s)""" % (
        " " * indent,
        plate.OMERO_CLASS,
        plate.getId(),
        plate.getName(),
        plate.getOwnerOmeName()))



# Definitions for screen and plate lists for specified user and group

def list_screens(my_exp_id, default_group_id):
    Screen_list=list()

    for screen in conn.getObjects("Screen", opts={'owner': my_exp_id,
                                                'group': default_group_id,
                                                'order_by': 'lower(obj.name)',
                                                'limit': -1, 'offset': 0}):
        Screen_list.append("""%s:%s  Name:"%s" (owner=%s)""" % (
            screen.OMERO_CLASS,
            screen.getId(),
            screen.getName(),
            screen.getOwnerOmeName()))
    return Screen_list

def list_plates(my_exp_id, default_group_id):    
    Plate_list=list()
    for plate in conn.getObjects("Plate", opts={'owner': my_exp_id,
                                                'group': default_group_id,
                                                'order_by': 'lower(obj.name)',
                                                'limit': -1, 'offset': 0}):
        Plate_list.append("""%s:%s  Name:"%s" (owner=%s)""" % (
            plate.OMERO_CLASS,
            plate.getId(),
            plate.getName(),
            plate.getOwnerOmeName()))
    return Plate_list
        
        
def dict_plates(my_exp_id, default_group_id):        
    Plate_dict={}
    for plate in conn.getObjects("Plate", opts={'owner': my_exp_id,
                                                'group': default_group_id,
                                                'order_by': 'lower(obj.name)',
                                                'limit': -1, 'offset': 0}):
        Plate_dict.update({("""%s:%s  Name:"%s" (owner=%s)""" % (
            plate.OMERO_CLASS,
            plate.getId(),
            plate.getName(),
            plate.getOwnerOmeName())):plate.getId()})
    return Plate_dict
        

def dropdown_widget(Screen_list,
                    dropdown_widget_name,
                    displaywidget=False):

    Screen_sel = widgets.Dropdown(
        options=Screen_list,
        value=Screen_list[0],
        description=dropdown_widget_name,
        disabled=False,
    )
    if displaywidget is True:
        display(Screen_sel)
    return Screen_sel

def dropdown_widget(plate_in_Screen_list,
                    dropdown_widget_name,
                    displaywidget=False):

    plate_sel = widgets.Dropdown(
        options=plate_in_Screen_list,
        value=plate_in_Screen_list[0],
        description=dropdown_widget_name,
        disabled=False,
    )
    if displaywidget is True:
        display(plate_sel)
    return plate_sel

def dropdown_widget(plate_list,
                    dropdown_widget_name,
                    displaywidget=False):

    plate_sel = widgets.Dropdown(
        options=plate_list,
        value=plate_list[0],
        description=dropdown_widget_name,
        disabled=False,
    )
    if displaywidget is True:
        display(plate_sel)
    return plate_sel

def dropdown_widget_dict(plate_dict,
                    dropdown_widget_name,
                    displaywidget=False):

    plate_sel = widgets.Dropdown(
        options=plate_dict.keys(),
        value=plate_dict.keys()[0],
        description=dropdown_widget_name,
        disabled=False,
    )
    if displaywidget is True:
        display(plate_sel)
    return plate_sel


def myFunc(e):
    return e.getWellPos()

def myFunc2(e):
    wellp= e.getWellPos()
    number_idx = wellp[1:3]
    letter_idx = wellp[0]
    #str, num = substring(wellp, 1, number_idx-1), substring(wellp, number_idx, length(wellp))
    return letter_idx, int(number_idx)

# Connect to the server
def connect(hostname, username, password):
    conn = BlitzGateway(username, password,
                        host=hostname, secure=True)
    conn.connect()
    return conn

# Load-plate
def load_plate(conn, plate_id):
    return conn.getObject("Plate", plate_id)

# Load-pipeline
def load_pipeline(pipeline_path):
    pipeline = cpp.Pipeline()
    pipeline.load(pipeline_path)
    # Remove first 4 modules: Images, Metadata, NamesAndTypes, Groups...
    # (replaced by InjectImage module below)
    for i in range(4):
        print('Remove module: ', pipeline.modules()[0].module_name)
        pipeline.remove_module(1)
    print('Pipeline modules:')
    for module in pipeline.modules():
        print(module.module_num, module.module_name)
    return pipeline

# Save-results
def save_results(conn, files, plate):
    # Upload the CSV files
    print("saving results...")
    namespace = "cellprofiler.demo.namespace"
    for f in files:
        ann = conn.createFileAnnfromLocalFile(f, mimetype="text/csv",
                                              ns=namespace, desc=None)
        plate.linkAnnotation(ann)

#Part of analyzing a plate
def analyze_plate(plate, pipeline):
    warnings.filterwarnings('ignore')

    Nuclei = pd.DataFrame()
    files = {}
    files_names= list()
    pf=pd.DataFrame()

    # create list from generator
    ##RH: list exclude empty wells: well not containing any WellSample!
    wellss = list(plate.listChildren())
    print(plate.getId())
    wells = list()
    for w in wellss:
        #print()
        if w.sizeOfWellSamples() > 0:
            wells.append(w)
    #print(type(wells))

    wells.sort(key=myFunc2)


    # use the first 5 wells only for testing script
    if n_wells_to_analyze >0:
        wells = wells[0:n_wells_to_analyze]
    else:
        wells = wells
    well_count = len(wells)
    
    #folders maken voor elke field
    
    files, files_names = analyze_well(well_count, wells, files, files_names)
    cpprefs.set_default_output_directory(new_output_directory)
    print("analysis done")
    return files, files_names

#Part of analyzing a well
def analyze_well(well_count, wells, files, files_names):
    for count, well in enumerate(wells):
        print('Analyzing ... Well: %s/%s' % (count + 1, well_count), 'row:', well.row, 'column:', well.column , 'Wellpos:' , well.getWellPos())
        # Load a single Image per Well
        index = well.countWellSample()
        
        files, files_names = analyze_field(index, count, well, files, files_names, new_output_directory)
    
    return files, files_names
    
#Part of analyzing a field
def analyze_field(index, count, well, files, files_names, new_output_directory):
    for index in range(0, index):
        image = well.getImage(index)
        pixels = image.getPrimaryPixels()#what does this function do?
        size_c = image.getSizeC()

        # For each Image in OMERO, we copy pipeline and inject image modules
        pipeline_copy = pipeline.copy()
        pipeline_copy = get_plane(image, pixels, size_c, pipeline_copy)
        pipeline_copy.run()
        

        for f in os.listdir(new_output_directory):
            if f.endswith(".csv"):
                files_names.append(f)
                path=os.path.join(new_output_directory,f)
                pf=pd.read_csv(path, index_col=None, header=0)
                pf['Image'] = image.getId()
                pf['Well'] = well.getId()
                pf['WellName'] = well.getWellPos()
                pf['Field'] = index
                f_key = f+"_"+str(count)+str(index)
                files[f_key]=pf

    return files, files_names

#Getting the image plane
def get_plane(image, pixels, size_c, pipeline_copy):
    # Inject image for each Channel (pipeline only handles 3 channels)
    for c in range(0, size_c):
        plane = pixels.getPlane(0, c, 0)
        #print(plane)
        image_name = image.getName()
        #print(image_name)

        # Name of the channel expected in the pipeline
        if c == 0:
            image_name = image_name_C1
        if c == 1:
            image_name = image_name_C2
        if c == 2:
            image_name = image_name_C3

        inject_image_module = InjectImage(image_name, plane)
        inject_image_module.set_module_num(1)
        pipeline_copy.add_module(inject_image_module)
        
    return pipeline_copy


# Disconnect
def disconnect(conn):
    conn.close()


print("Definitions done!")


# ## Set Cell Output Directory
new_output_directory = os.path.normcase(tempfile.mkdtemp())
X = str(argv[1])
if X != "":
    Output_Path = str(X+new_output_directory)
    os.makedirs(Output_Path)
    new_output_directory = Output_Path
#new_output_directory = '/tmp/tmpd5amb_au' #If you want the output to constantly be in the same folder
cpprefs.set_default_output_directory(new_output_directory)


print(new_output_directory)


# <div class="alert alert-block alert-success">
# 
# ## Connect to OMERO@UL
# _**Does require input from user!**_


HOST = 'omero-t.services.universiteitleiden.nl'# test-server
#HOST = 'omero.services.universiteitleiden.nl'# uncomment for connection to the pilot server
conn = BlitzGateway(str(argv[2]),
                    str(argv[3]),
                    host=HOST, secure=True)
print(conn.connect())
conn.c.enableKeepAlive(60)



# ### User and group info
# _**Does not require input from user!**_
# #### Comments:
# You are logged into your "default group" in OMERO, which you can see on login in OMERO webserver. This can be changed by OMERO admins or by your self if you are in multiple groups.
# 
# If you want to analyse data from other users in your group, you need to adjust code below, ask for help with this in OEMRO Teams Channel.
user = conn.getUser()
print("Current user:")
print("   ID:", user.getId())
print("   Username:", user.getName())
print("   Full Name:", user.getFullName())
print("Current group:")
print("   Group ID:", conn.getEventContext().groupId)
print("   Group Name:", conn.getEventContext().groupName)


my_exp_id = conn.getUser().getId()
default_group_id = conn.getEventContext().groupId

#When using other users data change id's below
my_exp_id = 3 #on test server matthijs is 7, rh=3
default_group_id = 5 #omero-t: 5 is water_RA

#Below you can fill in the plate_id. does not requires interactive selection from dropdown list
plate_id = 201 #this will overwrite the selected value in dropdown list 

plate = conn.getObject("Plate", plate_id)
#print("Folowing plate is selecte for analysis: ", print_plateInfo(plate))

print("plate id selected is: ",plate_id)
print("Plate: ", plate.getName())


# ## Load and inspect CP pipline
# _**Requires input from user!**_
# 
# #### Input:
# * Path to CellProfiler .cppipe file
# #### Comments:
# 
# * The first 4 modules are removed (e.g. Images, Metadata, NamesAndTypes and Groups)
# * Can also made selectable popup, need to be tested for windows environment, works in Linux.

pipeline_path = str(argv[4])
#pipeline_path = "/home/robbert/OmeroGit/omero-cellprofiler/notebooks/pipelines/CP_mv38_rhv3.cppipe"
#pipeline_path = "/home/rohola/gitlapRepos/omero-cellprofiler/notebooks/pipelines/CP_mv38_rhv3.cppipe"
pipeline = load_pipeline(pipeline_path)


# <div class="alert alert-block alert-success">
# 
# ### Run Cell Profiler Pipeline on the plate
# 
# _**Requires input from user!**_
# 
# #### Input:
# * The image names used in the pipeline must be the same as in the code, user should paste each name used for each channel in this step. 
# * number of well that need to be analyzed must be indicated. 
#     * Use a negative value for analyzing whole plate.
#     * Use a positive value for testing first few wells.
# #### Comments:
# 
# 
# 
# ##### issues: 
# * The channel names can be extracted from metadata template ImageAcquisition sheet, "Channel labels". This is not consistent yet. 
# 
# * The image names used in CP-pipeline can be also extracted, need to look into that.
# 
# #### Output:
# * Printing progress per well being analyzed. 

#To Do: 1. done! change CP csv export instead of hdf5 file done!
#To Do: 2. done! change statement to show C4 instead of columns etc ..print('Well: %s/%s' % (count + 1, well_count), 'row:', well.row, 'column:', well.column)
#To Do 3. check how csv file is exported well numbers or cols need to merge them with user metadata 
#To Do 4. interactively select image_name for each channel from template or CP-pipeline

# image names used in pipeline ".cppipe" file
image_name_C1= 'Nuclei'
image_name_C2= 'image_PI'
image_name_C3= 'image_AnV'
image_name_C4= 'none'

n_wells_to_analyze = int(argv[5])
#n_wells_to_analyze= 5 #for testing purpose you can select first few wells to be tested. Use "-1" for all plates 

# Analyze-data

def unique(list1): 
      
    # insert the list to the set 
    list_set = set(list1) 
    # convert the set to the list 
    unique_list = (list(list_set)) 
    """for x in unique_list: 
        print(x)"""
    return(unique_list)

def concat_CSVs(files_u, files):
    print("Saving concatenated csv files....")
    if not os.path.exists(os.path.join(new_output_directory,"ConCatCSVs")):
        os.makedirs(os.path.join(new_output_directory,"ConCatCSVs"))
    new_output_directory_concat= os.path.join(new_output_directory,"ConCatCSVs")
    
    concated_files= {}
    files_csv_list_con_fullPath= list()
    
    
    for j in files_u:
        print("j:" + j)
        mainCSV_list=list()
        mainName=j[0:len(j)-4]+"_concat.csv"

        for i in files:
            if j in i:
                mainCSV_list.append(files[i])
        mainCSV=pd.concat(mainCSV_list, ignore_index=True)
        #print(mainCSV)
        path1 = os.path.join(new_output_directory_concat , mainName)
        print("csv's concatenated for : "+path1)
        mainCSV.to_csv(path1, index=False)
        #print(mainName)
        #mainCSV=
    for f in os.listdir(new_output_directory_concat):
                if f.endswith(".csv"):
                    #files_names.append(f)
                    path=os.path.join(new_output_directory_concat,f)
                    pf=pd.read_csv(path, index_col=None, header=0)
                    f_key = f+"_pd"
                    concated_files[f_key]=pf
    print("Done saving concatenated csv files!")
    files_list_con= os.listdir(new_output_directory_concat)
    #print(files_list_con)
    for f in files_list_con:
        if f.endswith(".csv"):
            fullPath= os.path.join(new_output_directory_concat, f)
            files_csv_list_con_fullPath.append(fullPath)
    return files_list_con, concated_files, files_csv_list_con_fullPath
    
files, files_names = analyze_plate(plate, pipeline)
files_u = unique(files_names)
files_list_con, concated_files, files_csv_list_con_fullPath = concat_CSVs(files_u, files)


# ### Save csv files from CP analysis 
# 

# In[22]:


#print(len(files), len(files_names))

def unique(list1): 
      
    # insert the list to the set 
    list_set = set(list1) 
    # convert the set to the list 
    unique_list = (list(list_set)) 
    """for x in unique_list: 
        print(x)"""
    return(unique_list)

files_u=unique(files_names)
#print(type(files_u), len(files_u))
#print(files_u)
if not os.path.exists(os.path.join(new_output_directory,"ConCatCSVs")):
    os.makedirs(os.path.join(new_output_directory,"ConCatCSVs"))
new_output_directory_concat= os.path.join(new_output_directory,"ConCatCSVs")
for j in files_u:
    print("j:" + j)
    mainCSV_list=list()
    mainName=j[0:len(j)-4]+"_concat.csv"
    
    for i in files:
        if j in i:
            mainCSV_list.append(files[i])
    mainCSV=pd.concat(mainCSV_list, ignore_index=True)
    #print(mainCSV)
    path1 = os.path.join(new_output_directory_concat , mainName)
    print("csv's concatenated for : "+path1)
    mainCSV.to_csv(path1, index=False)
    #print(mainName)
    #mainCSV=
print("Done saving concatenated csv files!")
files_list_con= os.listdir(new_output_directory_concat)
print(files_list_con)


# ### Close the connection to the server
print(type(files_list_con))
print(type(concated_files))
print(files_list_con)
#print(concated_files) #large output
print(files_csv_list_con_fullPath)


conn.close()

