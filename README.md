# CP analysis on plate data in OMERO@UL

CP analysis is performed using CP3.1.9 pipelines (.cppipe), and later CP4.1.3 on plate data in OMERO server. 
Further instructions for analysis is provided in the jupyter notebooks. 

# Branches

No Other branches

# Content in the Master Branch

- Conda-env

In environment.yml are all conda packages listed that were needed to correctly run jupyter notebook with. The environment.yml in older branches has packages that were used with Python2, while the file in the master branch has packages for python3.

- notebooks

`20201027_rh_mv038-Definitions.ipynb` is a python3 notebook where the analysis-functions have been put in the definitions cell. This is the most recent notebook need to be further checked.


`PythonCellProfiler.py` and `PythonCellProfilerArgv.py` are python scripts based on the Definitions-notebook that were taken to test. 
